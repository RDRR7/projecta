#define COLOR (((bgcolor << 4)  | fgcolor) << 8)

#include "screen.h"

static uint8_t fgcolor, bgcolor, row, column;
static uint16_t *cursor;

void clear_screen() {
   	uint16_t data=COLOR | ' ';
   	uint16_t *vgaptr;
	for(uint16_t i=VGA_START_ADDR; i<=VGA_END_ADDR; i+=2) {
      vgaptr=(uint16_t*)i;
      *vgaptr=data;
  	}
  	set_cursor(0, 0);
};

void set_cursor(uint8_t _row, uint8_t _column) {
	if(_row>=0 && _row<=29 && _column>=0 && _column<=79) {
		row=_row;
		column=_column;
		int16_t tmp=80*_row+_column;
		cursor=VGA_START_ADDR;
		for(int16_t i=0; i<tmp; i++)
			cursor++;
	}
};

void get_cursor(uint8_t *_row, uint8_t *_column) {
	*_row=row;
	*_column=column;
};

void set_color(uint8_t _fgcolor, uint8_t _bgcolor) {
	if(_fgcolor>=0 && _fgcolor<=15 && _bgcolor>=0 && _bgcolor<=15) {
		fgcolor=_fgcolor;
		bgcolor=_bgcolor;
	}
};

void get_color(uint8_t *_fgcolor, uint8_t *_bgcolor) {
	*_fgcolor=fgcolor;
	*_bgcolor=bgcolor;
};

void put_char(uint8_t _ch) {
	if(_ch=='\n') {
		row++;
		column=0;
	}
	else {
		*cursor=COLOR | _ch;
		column++;
		if(column>=80) {
			row++;
			column=0;
		}
	}
	if(row>=30)
		row=0;
	set_cursor(row, column);
}; 

void puts(char *_str) {
	int i=0;
	while(_str[i]!='\0')
		put_char(_str[i++]);
};


int division(int _dividend, int _divisor) {
	int tmp=0;
	while(_dividend>=_divisor) {
		_dividend-=_divisor;
		tmp++;
	}
	return tmp;
};

int mod(int _dividend, int _divisor) {
	while(_dividend>=_divisor)
		_dividend-=_divisor;
	return _dividend;
};

void reverse(char s[]) {
	int i, j, length=0;
	char c;

	while(s[length]!='\0')
		length++;

	for (i=0, j=length-1; i<j; i++, j--) {
		c=s[i];
		s[i]=s[j];
		s[j]=c;
	}
}

void itoa(int n, char s[]) {
	int i=0;
	do {
		s[i++]=mod(n, 10)+'0';
		n=division(n, 10);
	} while (n>0);
	s[i]='\0';
	reverse(s);
}

void put_decimal(uint32_t _n) {
	char s[32];
	itoa(_n, s);
	puts(s);
};