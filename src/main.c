#include "screen.h"

int main() {
	uint8_t x=10, y=10, fg=14, bg=1;
	
	clear_screen();
	set_color(fg, bg);
	put_char('a');
	
	set_cursor(y, x);
	put_char('1');

	get_cursor(&y, &x);

	fg=3; bg=2;
	get_color(&fg, &bg);
	puts("fg: ");
	put_decimal(fg);
	puts("bg: ");
	put_decimal(bg);

	
	puts("X: ");
	put_decimal(x);
	puts("Y: ");
	put_decimal(y);

	put_char('-');
	puts("Hola Mundo!");
	put_char('\n');
	put_decimal(123);

	get_cursor(&y, &x);
	puts("X: ");
	put_decimal(x);
	puts("Y: ");
	put_decimal(y);

	set_cursor(29, 79);
	put_char('a');
	
	/*set_color(12, 10);
	clear_screen();
	for(int x=0; x<80; x++)
		for(int y=0; y<30; y++)
			if(y==0 || y==29 || x==0 || x==79) {
				set_cursor(y, x);
				put_char('o');
			}

	set_cursor(14, 27);
	put_decimal(10101);
	puts(" Hola Mundo! ");
	put_decimal(10101);*/

	return 0;
}